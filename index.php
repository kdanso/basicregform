<!DOCTYPE HTML>
<?php
session_start();
require_once 'connect.php';

//Upon Submission click, insert values into database. Redirect to results page. 
if (isset($_POST['submit']))
{
	$firstName = $_POST['firstName'];
	$lastName = $_POST['lastName'];
	$dateOfBirth = $_POST['dateOfBirth'];
	$q = "INSERT INTO regUser (firstName, lastName, dateOfBirth) VALUES(:firstName, :lastName, :dateOfBirth);";
	$queryPrep = $conn->prepare($q);
	
	//execute array
	$result = $queryPrep->execute(array(
		":firstName" => $firstName,
		":lastName" => $lastName,
		":dateOfBirth" => $dateOfBirth
		));
	
	//redirect to result page
	echo '<script>window.location = "result.php" </script>';

}

?>

<html>
	<head>
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/main.css">
			<body>
			
				<form method="POST" action="">
					<div class="box">
						<h1>Oakbrook Registration</h1>

						First Name <input type="text" id="firstName" name="firstName" class="button-field" />
						Last Name <input type="text" id="lastName" name="lastName" class="button-field" />	
						Date of Birth <input type="date" id="dateOfBirth" name="dateOfBirth" class="button-field" />
						<br>
						<br>
						<form>
						<input type="submit" name="submit" value="Register"/>
						</form>
						<br>
						<br>
					  </div>
						<br>
							<form>
							<button type="submit" formaction="registeredUsers.php">Registered Users</button>
							</form>
								
				  
				</form>

			</body>
	</head>
</html>