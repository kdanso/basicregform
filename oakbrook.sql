-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 173.194.105.207
-- Generation Time: Jan 07, 2019 at 03:14 PM
-- Server version: 5.6.39
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oakbrook`
--

-- --------------------------------------------------------

--
-- Table structure for table `regUser`
--

CREATE TABLE `regUser` (
  `regUserID` int(5) NOT NULL,
  `firstName` varchar(30) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `dateOfBirth` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regUser`
--

INSERT INTO `regUser` (`regUserID`, `firstName`, `lastName`, `dateOfBirth`) VALUES
(4, 'Kofi', 'Danso', '1995-08-18'),
(19, 'George', 'Best', '1949-12-26'),
(21, 'Jason', 'Bourne', '1991-01-01'),
(73, 'Ian', 'Wright', '1960-01-01'),
(75, 'Karim', 'Benzema', '1987-12-19'),
(77, 'Toby', 'Alderweireld', '1989-03-02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `regUser`
--
ALTER TABLE `regUser`
  ADD PRIMARY KEY (`regUserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `regUser`
--
ALTER TABLE `regUser`
  MODIFY `regUserID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
