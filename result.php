<!DOCTYPE HTML>
<?php
session_start();
require_once 'connect.php';
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/main.css">
			<body>
				<h1>Result</h1>
					<?php //API call and request
							
						// Increase time limit for script
						set_time_limit(300);

						// API function, requires url, data array, retries (optional), verbose (optional)
						function api($url, $data, $retries=10, $verbose=False)
						{
							  $attempt = 0;
							  while($attempt<$retries)
							  {
								$enc_data = json_encode($data);                            // Encode json array
								$curl_handle = curl_init($url);                            // Start
								curl_setopt($curl_handle, CURLOPT_POST, 1);                // Post
								curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $enc_data);  // Set data
								curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));   // Set content type
								curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);   // Don't print result
								$result_enc = curl_exec($curl_handle);                     // Execute
								curl_close($curl_handle);
								$result = json_decode($result_enc, true);                  // Decode to array    

								// If an array, and accepted, end and return True
								if(is_array($result))
								{
								  // If there is a decision
								  if(array_key_exists('DecisionResult', $result))
								  {
									if($verbose){echo "Success attempt " . $attempt . " " . $result['DecisionResult'] . "<br />";}
									return $result['DecisionResult'];
								  }
								  // If the input was invalid, just exit and return message
								  if(array_key_exists('Message', $result) && $result['Message'] == "The request is invalid.")
								  {
									if($verbose){echo "Failed attempt " . $attempt . " " . $result['Message'] . " <br />";}
									return $result['Message'];
								  }
								  // Otherwise, continue (and print out if verbose)
								  if($verbose){echo "Failed attempt " . $attempt . " " . $result['Message'] . " <br />";}
								}

								// Increment attempt counter
								$attempt++;
							  }
							  // If too many retries, fail and return False
							  return False;
							}

							// Set url
							$api_url = 'http://dummydecisionengine.azurewebsites.net/decision';

							// Make array
							$data = array();
							$data['firstName'] = 'myfirstname';
							$data['lastName'] = 'mylastname';
							$data['dateOfBirth'] = '01/01/1980';

							// Call function
							$result = api($api_url, $data, 10, True);
							echo $result;
					?>
					<br>
					<p>Registration successful!</p>

				<a href="index.php"><div id="home" align="center">Return Home</div></a>
			</body>
	</head>
</html>
	


	